const http = require('http');
const path = require('path');
const fs = require('fs');
const qs = require('querystring');

const router = require('./router')

const router1 = new router();

http.createServer((request,response) => {
    let filepath = path.join(__dirname,'page', request.url === '/' ? 'index.html' : request.url);

    if(request.url.match('\.css$')) router1.css(filepath,request,response);
    else if (request.url.match('\.png$')) router1.image(filepath,request,response);
    else if(request.url.match('\.js$')) router1.script(filepath,request,response);
    else router1.html(filepath,request,response);

  }).listen(8000);
