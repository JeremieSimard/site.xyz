
(function(window, document, undefined){

// code that should be taken care of right away

window.onload = init;

  function init(){
    // the code to be called when the dom has loaded
    // #document has its nodes
    var test = document.getElementById('test');

    test.animate([
      { transform: 'scale(1)'},
      { transform: 'scale(1) rotate(360deg)'},
    ], {
      duration: 2000,
      easing: 'linear',
      delay: 10,
      iteration: Infinity,
      direction: 'normal',
      fill: 'forwards',
    });
  }

})(window, document, undefined);
