const path = require('path');
const fs = require('fs');

class Router {
  constructor(){}

  html(filepath, request, response)
  {
    fs.readFile(filepath, (err,content) =>{
      if(err){
        if(err.code == 'ENOENT'){
          fs.readFile(path.join(__dirname,'page','404.html'), (err, content) =>{
            response.writeHead(404,{'Content-Type':'text/html'});
            response.end(content);
          });
        }else {
          response.writeHead(500);
          response.end(`Server Error, code ${err.code}`);
        }
      }else {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(content,'utf8');
        response.end();
      }
    });
  }

  css(filepath, request, response)
  {
    fs.readFile(filepath, (err,content) =>{
      if(!err){
        response.writeHead(200, {'Content-Type': 'text/css'});
        response.write(content,'utf8');
        response.end();
      }
    });
  }

  image(filepath, request, response)
  {
    fs.readFile(filepath, (err,content) =>{
      if(!err){
        response.writeHead(200, {'Content-Type': 'image/png'});
        response.write(content);
        response.end();
      }
    });
  }

  script(filepath, request, response)
  {
    fs.readFile(filepath, (err,content) =>{
      if(!err){
        response.writeHead(200, {'Content-Type': 'text/javascript'});
        response.write(content);
        response.end();
      }
    });
  }
}

module.exports = Router;
